package com.example.json.experiments;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.example.json.experiments.filters.JsonViewDefinitionFilter;
import com.example.json.experiments.filters.JsonViewDefinitionResourceLoader;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer customizeObjectMapper(ResourceLoader resourceLoader) {
		return new Jackson2ObjectMapperBuilderCustomizer() {
			
			@Override
			public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
				jacksonObjectMapperBuilder.defaultViewInclusion(true);
				JsonViewDefinitionFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
				theFilter.setDefaultViewInclusion(true);
				FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);
				jacksonObjectMapperBuilder.filters(filters);
				jacksonObjectMapperBuilder.modules(new Hibernate5Module());
			}
		};
	}

}
