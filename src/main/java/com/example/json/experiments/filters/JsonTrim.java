package com.example.json.experiments.filters;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonTrim {

	ObjectMapper mapper = new ObjectMapper();

	public JsonNode trim(String jsonSpec, String jsonData) throws IOException {
		JsonNode nodeSpec = mapper.readTree(jsonSpec);
		JsonNode nodeData = mapper.readTree(jsonData);
		process(nodeSpec, nodeData);
		return nodeData;
	}

	private void process(JsonNode nodeSpec, JsonNode nodeData) {
		Iterator<Entry<String, JsonNode>> fields = nodeData.fields();
		while(fields.hasNext()) {
			Entry<String, JsonNode> next = fields.next();
			String key = next.getKey();
			JsonNode value = next.getValue();
			if (!nodeSpec.has(key)) {
				((ObjectNode)nodeData).remove(key);
			} else if (value.isArray()) {
				Iterator<JsonNode> elements = value.elements();
				JsonNode elSpec = nodeSpec.get(key).get(0);
				while (elements.hasNext()) {
					JsonNode elData = elements.next();
					process(elSpec, elData);
				}
			} else if (value.isObject()) {
				process(nodeSpec.get(key), value);
			}
		}
	}

}
