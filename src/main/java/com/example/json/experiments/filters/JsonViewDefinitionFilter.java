package com.example.json.experiments.filters;


import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.annotation.JsonClassDescription;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;
import net.minidev.json.JSONArray;

// @see http://www.baeldung.com/jackson-serialize-field-custom-criteria
@CommonsLog
@RequiredArgsConstructor
public class JsonViewDefinitionFilter extends SimpleBeanPropertyFilter {

	public final static String JSON_VIEW_DEFINITION_FILTER = "JsonViewDefinitionFilter";
	
	@Setter
	@Getter
	private boolean defaultViewInclusion = false;

	// TODO Pass the state with params piggy back Decorator on PropertyWriter to store the path (or store it in ThreadLocal)
	// State! This filter can't be called concurrently
	String path = "$";
	
	private final JsonViewDefinitionResourceLoader jsonViewDefinitionResourceLoader;
	
	// Enforcing synchronized to prevent mixing up the path on different objects
	@Override
	public synchronized void serializeAsField(Object pojo, JsonGenerator jgen, SerializerProvider provider, PropertyWriter writer)
			throws Exception {
		Class<?> activeView = provider.getActiveView();
		
		if (activeView != null) {
			String propertyName = writer.getName();
			if (checkPath(activeView, propertyName)) {
				nest(writer);
				writer.serializeAsField(pojo, jgen, provider);
				unnest(writer);
			}
		} else if (this.defaultViewInclusion) {
			writer.serializeAsField(pojo, jgen, provider);
		}
	}

	private void unnest(PropertyWriter writer) {
		int sepLength = 1;
		if (writer.getType().isArrayType() || writer.getType().isCollectionLikeType()) {
			sepLength = 4;
		}
		this.path = this.path.substring(0, this.path.length() - writer.getName().length() - sepLength);
	}

	private void nest(PropertyWriter writer) {
		this.path += "." + writer.getName();
		if (writer.getType().isArrayType() || writer.getType().isCollectionLikeType()) {
			path += "[*]";
		}
	}
	private Map<Class<?>, Map<String, Boolean>> cache = new HashMap<>();

	// TODO Move checkPath to another class
	private boolean checkPath(Class<?> activeView, String propertyName) throws IOException {
		String fullPath = path + "." + propertyName;
		log.debug("Cheking path: " + fullPath);
		Map<String, Boolean> map = cache.get(activeView);
		if (map != null) {
			Boolean isPathInViewDefinition = map.get(fullPath);
			if (isPathInViewDefinition != null) {
				return isPathInViewDefinition;
			}
		} else {
			cache.put(activeView, new HashMap<>());
		}
		
		String jsonDefinition = jsonViewDefinitionResourceLoader.loadJsonViewDefinition(activeView);
		DocumentContext parse = JsonPath.parse(jsonDefinition);
		boolean checkPathResult = true;
		try {
			Object result = parse.read(fullPath);
			if (result == null) {
				checkPathResult = false;
			}
			if (result.getClass().isAssignableFrom(JSONArray.class) && ((JSONArray)result).isEmpty()) {
				checkPathResult = false;
			}
		} catch (PathNotFoundException e) {
			checkPathResult = false;
		}
		cache.get(activeView).put(fullPath, checkPathResult);
		return checkPathResult;
	}

	@Override
	protected boolean include(BeanPropertyWriter writer) {
		return true;
	}

	@Override
	protected boolean include(PropertyWriter writer) {
		return true;
	}
}