package com.example.json.experiments.filters;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.ResourceLoader;
import org.springframework.util.StreamUtils;

import com.fasterxml.jackson.annotation.JsonClassDescription;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class JsonViewDefinitionResourceLoader {
	
	private final ResourceLoader resourceLoader;
	
	@Getter
	private Map<Class<?>, String> cache = new HashMap<>();
	
	public String loadJsonViewDefinition(Class<?> activeView) throws IOException {
		String jsonDefinition = cache.get(activeView);
		if (jsonDefinition != null) {
			return jsonDefinition;
		}
		JsonClassDescription annotation = activeView.getAnnotation(JsonClassDescription.class);
		jsonDefinition = StreamUtils.copyToString(resourceLoader.getResource(annotation.value()).getInputStream(), Charset.forName("utf8"));
		this.cache.put(activeView, jsonDefinition);
		return jsonDefinition;
	}

}
