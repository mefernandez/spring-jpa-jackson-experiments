package com.example.json.experiments.filters.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.json.experiments.filters.JsonTrim;
import com.example.json.experiments.filters.data.Employee;
import com.example.json.experiments.filters.repository.EmployeeRepository;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository repository;
	
	@Autowired
	private ObjectMapper mapper; 
	
	@JsonView(NamesOnlyView.class)
	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> findAll() {
		List<Employee> employees = repository.findAll();
		return ResponseEntity.ok(employees);
	}

	@JsonView(NamesOnlyView.class)
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> findOne(@PathVariable Long id) {
		Employee employee = repository.findOne(id);
		return ResponseEntity.ok(employee);
	}

	@JsonView(NamesAndSalariesView.class)
	@GetMapping("/employees/{id}/full")
	public ResponseEntity<Employee> findOneFull(@PathVariable Long id) {
		Employee employee = repository.findOne(id);
		return ResponseEntity.ok(employee);
	}

	@JsonView(NamesOnlyView.class)
	@PostMapping("/employees")
	public ResponseEntity<?> save(@RequestBody Employee employee) {
		repository.save(employee);
		return ResponseEntity.created(null).build();
	}

//	@JsonView(NamesOnlyView.class)
//	@PutMapping("/employees/{id}")
//	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Employee employee) {
//		repository.save(employee);
//		return ResponseEntity.noContent().build();
//	}
//
//	@JsonView(NamesOnlyView.class)
//	@PatchMapping("/employees/{id}")
//	public ResponseEntity<?> patch(@PathVariable Long id, @RequestBody Employee changes) {
//		Employee employee = repository.findOne(id);
//		ObjectMerger merger = new ObjectMerger();
//		merger.merge(changes, employee, employee);
//		repository.save(employee);
//		return ResponseEntity.noContent().build();
//	}

	@JsonView(NamesOnlyView.class)
	@PatchMapping("/employees/{id}")
	public ResponseEntity<?> patch(@PathVariable Long id, @RequestBody String json) throws JsonProcessingException, IOException {
		JsonTrim trimmer = new JsonTrim();
		JsonNode trimmedJson = trimmer.trim("{ \"name\": true }", json);
		Employee employee = repository.findOne(id);
		mapper.readerForUpdating(employee).readValue(trimmedJson);
		repository.save(employee);
		return ResponseEntity.noContent().build();
	}
}
