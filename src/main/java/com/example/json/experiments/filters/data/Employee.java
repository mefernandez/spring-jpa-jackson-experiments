package com.example.json.experiments.filters.data;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.example.json.experiments.filters.JsonViewDefinitionFilter;
import com.example.json.experiments.filters.controller.NamesOnlyView;
import com.example.json.experiments.jpa.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Entity
@SequenceGenerator(name="default_gen", sequenceName="employee_id_seq", allocationSize=100)
@JsonFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER)
public class Employee extends BaseEntity {
	
	//@JsonView({JsonDefinedView.class})
	private String name;

	// Sensitive!
	private String pass;

	// Sensitive
	@OneToMany
	private Set<Salary> salaries;
	
	//@JsonView(JsonDefinedView.class)
	@ManyToOne
	private Employee boss;

	public void addSalary(Salary salary) {
		if (this.salaries == null) {
			this.salaries = new HashSet<>();
		}
		this.salaries.add(salary);
	}

}
