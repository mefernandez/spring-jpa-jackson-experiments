package com.example.json.experiments.filters.data;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;

import com.example.json.experiments.filters.JsonViewDefinitionFilter;
import com.example.json.experiments.jpa.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFilter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@SequenceGenerator(name="default_gen", sequenceName="salary_id_seq", allocationSize=100)
@JsonFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER)
public class Salary extends BaseEntity {

	private BigDecimal amount;
	private Date fromDate;
	private Date toDate;
}
