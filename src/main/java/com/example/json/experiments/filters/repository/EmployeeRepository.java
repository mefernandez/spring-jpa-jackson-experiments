package com.example.json.experiments.filters.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.json.experiments.filters.controller.NamesOnlyView;
import com.example.json.experiments.filters.data.Employee;
import com.fasterxml.jackson.annotation.JsonView;

@RepositoryRestResource(path = "employees" , collectionResourceRel = "employees", itemResourceRel = "employee")
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	
	@JsonView(NamesOnlyView.class)
	@Override
	Page<Employee> findAll(Pageable arg0);

}
