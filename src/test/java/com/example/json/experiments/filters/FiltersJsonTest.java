package com.example.json.experiments.filters;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.json.experiments.filters.controller.NamesAndSalariesView;
import com.example.json.experiments.filters.controller.NamesOnlyView;
import com.example.json.experiments.filters.data.Employee;
import com.example.json.experiments.filters.data.Salary;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

@RunWith(SpringRunner.class)
@JsonTest
public class FiltersJsonTest {

	@Autowired
	private JacksonTester<Employee> json;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Before
	public void setup() {
		mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}
	
	@Test
	public void testNamesOnlyView() throws Exception {
		// Data
		Employee mark = new Employee();
		mark.setName("Mark");
		Employee boss = new Employee();
		boss.setName("Bob");
		mark.setBoss(boss);

		// Setup
		PropertyFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
		FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);

		// Test
		String dtoAsString = mapper.writer(filters).withView(NamesOnlyView.class).writeValueAsString(mark);

		// Assert
		JsonContent<Employee> exp = new JsonContent<Employee>(this.getClass(), ResolvableType.forClass(Employee.class), dtoAsString);
		assertThat(exp).extractingJsonPathStringValue("@.name").isEqualTo("Mark");
		assertThat(exp).extractingJsonPathStringValue("@.boss.name").isEqualTo("Bob");
	}

	@Test
	public void testNamesAndSalariesView() throws Exception {
		// Data
		Employee mark = new Employee();
		mark.setName("Mark");
		DateFormat dateInstance = new SimpleDateFormat("dd/MM/yyyy");
		dateInstance.setTimeZone(TimeZone.getTimeZone("GMT"));
		mark.addSalary(new Salary(new BigDecimal("1234.56"), dateInstance.parse("01/01/2017"), dateInstance.parse("31/07/2017")));
		Employee boss = new Employee();
		boss.setName("Bob");
		mark.setBoss(boss);

		// Setup
		PropertyFilter theFilter = new JsonViewDefinitionFilter(new JsonViewDefinitionResourceLoader(resourceLoader));
		FilterProvider filters = new SimpleFilterProvider().addFilter(JsonViewDefinitionFilter.JSON_VIEW_DEFINITION_FILTER, theFilter);

		// Test
		String dtoAsString = mapper.writer(filters).withView(NamesAndSalariesView.class).writeValueAsString(mark);
		System.out.println(dtoAsString);
		// Assert
		JsonContent<Employee> exp = new JsonContent<Employee>(this.getClass(), ResolvableType.forClass(Employee.class), dtoAsString);
		assertThat(exp).extractingJsonPathStringValue("@.name").isEqualTo("Mark");
		assertThat(exp).extractingJsonPathNumberValue("@.salaries[0].amount").isEqualTo(1234.56);
		assertThat(exp).extractingJsonPathStringValue("@.salaries[0].fromDate").isEqualTo("2017-01-01T00:00:00.000+0000");
		assertThat(exp).extractingJsonPathStringValue("@.salaries[0].toDate").isEqualTo("2017-07-31T00:00:00.000+0000");
		assertThat(exp).extractingJsonPathStringValue("@.boss.name").isEqualTo("Bob");
		assertThat(exp).doesNotHaveJsonPathValue("@.boss.salaries");
	}
}
