package com.example.json.experiments.filters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;

public class JsonTrimTest {
	
	

	@Test
	public void testRemoveUnknownProperty() throws Exception {
		String jsonSpec = "{ \"name\": true }";
		String jsonData = "{ \"name\": \"Mark\", \"pass\": \"Secret\" }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("pass"));
	}

	@Test
	public void testRemoveUnknownNestedProperty() throws Exception {
		String jsonSpec = "{ \"name\": true }";
		String jsonData = "{ \"name\": \"Mark\", \"boss\": { \"name\": \"Bob\" } }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("boss"));
	}

	@Test
	public void testNestedArrayProperty() throws Exception {
		String jsonSpec = "{ \"name\": true, \"salaries\":  [ { \"fromDate\": true } ] }";
		String jsonData = "{ \"name\": \"Mark\", \"salaries\": [ { \"fromDate\": \"2017-01-01\", \"amount\": 100 } ] }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("salaries").get(0).get("amount"));
		assertEquals("2017-01-01", nodeData.get("salaries").get(0).get("fromDate").asText());
	}

	@Test
	public void testNestedObjectProperty() throws Exception {
		String jsonSpec = "{ \"name\": true, \"salary\":  { \"fromDate\": true } }";
		String jsonData = "{ \"name\": \"Mark\", \"salary\": { \"fromDate\": \"2017-01-01\", \"amount\": 100 } }";
		JsonTrim jsonTrim = new JsonTrim();
		JsonNode nodeData = jsonTrim.trim(jsonSpec, jsonData);
		assertEquals("Mark", nodeData.get("name").asText());
		assertNull(nodeData.get("salary").get("amount"));
		assertEquals("2017-01-01", nodeData.get("salary").get("fromDate").asText());
	}
}
