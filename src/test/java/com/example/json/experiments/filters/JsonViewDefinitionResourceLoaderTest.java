package com.example.json.experiments.filters;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.json.experiments.filters.controller.NamesOnlyView;

@RunWith(SpringRunner.class)
public class JsonViewDefinitionResourceLoaderTest {
	
	public interface TestView {
		
	}
	
	@Autowired
	private ResourceLoader resourceLoader;

	@Test
	public void loadJsonViewDefinitionFromJsonResourceCachesResource() throws IOException {
		JsonViewDefinitionResourceLoader loader = new JsonViewDefinitionResourceLoader(resourceLoader);
		assertTrue(loader.getCache().isEmpty());
		String jsonViewDefinition = loader.loadJsonViewDefinition(NamesOnlyView.class);
		assertEquals(48, jsonViewDefinition.length());
		assertEquals(48, loader.getCache().get(NamesOnlyView.class).length());
	}

	@Test
	public void loadJsonViewDefinitionFromCache() throws IOException {
		JsonViewDefinitionResourceLoader loader = new JsonViewDefinitionResourceLoader(resourceLoader);
		assertTrue(loader.getCache().isEmpty());
		loader.getCache().put(TestView.class, "Just a test");
		String jsonViewDefinition = loader.loadJsonViewDefinition(TestView.class);
		assertEquals("Just a test", jsonViewDefinition);
	}
}
